#!/usr/bin/python3

import time
import serial
import math
import signal 
import board
import busio
import adafruit_ads1x15.ads1015 as ADS
from adafruit_ads1x15.ads1x15 import Mode
from adafruit_ads1x15.analog_in import AnalogIn

lower_limit = 0
upper_limit = 100

operating_point = 20
adc_mean = 21000
scaling = 0.02

RATE = 3300

# import serial device
ser = serial.Serial('/dev/ttyACM0', 2000000, timeout=0.0)
if not (ser.is_open):
    print("Serial error")
    exit(1)

# Create the I2C bus with a fast frequency
i2c = busio.I2C(board.SCL, board.SDA, frequency=1000000)

# Create the ADC object using the I2C bus
ads = ADS.ADS1015(i2c)

# Create single-ended input on channel 0
chan0 = AnalogIn(ads, ADS.P0, ADS.P1)

# ADC Configuration
ads.mode = Mode.CONTINUOUS
ads.data_rate = RATE

lastTime = time.time()

def exit_handler(a,b):
    global ser
    ser.close()

signal.signal(signal.SIGINT, exit_handler)

time.sleep(1.0)

while True:
    if (time.time() - lastTime > 0.01):
        lastTime = time.time()

    data = ser.readline()[:-2] # Clear RX buffer to avoid slowing down

    # Calculate IIR mean of ADC
    adc_mean = 0.99* adc_mean + 0.01 * chan0.value

    # Old method: Map the complete range of ADC onto our limits
    # dimValue = int(lower_limit + (upper_limit - lower_limit) * chan0.value/32768.0)

    # New method: Add scaled AC ADC signal on top of predefined operating point
    dimValue = operating_point + scaling * (chan0.value - adc_mean)
    print ("Intensity: ", round(dimValue, 1))

    # Ensure that we stay within valid bounds
    dimValue = int(min(max(dimValue, lower_limit), upper_limit))
    ser.write(bytes([dimValue]))
